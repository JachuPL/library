DROP TABLE IF EXISTS Members;
DROP TABLE IF EXISTS Books;
DROP TABLE IF EXISTS Borrows;

CREATE TABLE Members (
  Id INTEGER IDENTITY(1, 1),
  FirstName VARCHAR(50) NOT NULL,
  MiddleName VARCHAR(50) NULL,
  LastName VARCHAR(100) NOT NULL,
  DateOfBirth DATE NOT NULL,
  PRIMARY KEY (Id)
);

CREATE TABLE Books (
  Id INTEGER IDENTITY(1, 1),
  Title VARCHAR(100) NOT NULL,
  Author VARCHAR(100) NOT NULL,
  ReleaseDate DATE NOT NULL,
  IsLostOrDestroyed BOOLEAN DEFAULT FALSE NOT NULL,
  PRIMARY KEY(Id)
);

CREATE TABLE Borrows (
  Id INTEGER IDENTITY(1, 1),
  MemberId INTEGER NOT NULL,
  BookId INTEGER NOT NULL,
  RentalDate DATE NOT NULL,
  ReturnDate DATE NOT NULL,

  PRIMARY KEY (Id),
  FOREIGN KEY (MemberId) REFERENCES Members(Id),
  FOREIGN KEY (BookId) REFERENCES Books(Id)
);