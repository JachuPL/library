INSERT INTO Members VALUES(1, 'Jan', 'Hubert', 'Bońkowski', '1995-09-13');
INSERT INTO Members VALUES(2, 'Marcin', NULL, 'Stankiewicz', '1978-03-28');
INSERT INTO Members VALUES(3, 'Paweł', NULL, 'Kovalsky', '1999-04-15');

INSERT INTO Books VALUES(1, 'Programowanie C#', 'Jesse Liberty', '2016-08-15', false);
INSERT INTO Books VALUES(2, 'Język C# 6.0 i platforma .NET 4.6', 'Andrew Troelsen', '2017-01-22', false);
INSERT INTO Books VALUES(3, 'LINQ to Objects w C# 4.0', 'Troy Magennis', '2012-03-21', false);
