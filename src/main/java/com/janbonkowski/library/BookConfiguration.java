package com.janbonkowski.library;

import com.janbonkowski.library.factory.BookFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.Date;

@Configuration
public class BookConfiguration {

    @Bean
    public BookFactoryBean bookFactoryBean(){
        BookFactoryBean bean= new BookFactoryBean();
        bean.setDate(new Date());
        return bean;
    }
}
