package com.janbonkowski.library.factory;

import com.janbonkowski.library.model.Book;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.Date;

public class BookFactoryBean implements FactoryBean<Book> {

    private String author;
    private String title;
    private Date releaseDate;

    public void setAuthor(String author){this.author = author;}
    public void setTile(String title){this.title=title;}
    public void setDate(Date date){this.releaseDate = date;}

    public BookFactoryBean(){
        this.author = "Gallus Anonimus";
        this.title = "Chronicles";
    }

    @PostConstruct
    public void setup() throws Throwable{
        System.out.printf("^^^^^^^^^^^^^^^^^^^^Instantiated BookFactoryBean\n");
        Assert.notNull(this.author, "Author cannot be null");
        Assert.notNull(this.title, "Title cannot be null");
    }

    @Override
    public Book getObject() throws Exception {
        return Book.Create(title, author, releaseDate);
    }

    @Override
    public Class<?> getObjectType() {
        return Book.class;
    }
}
