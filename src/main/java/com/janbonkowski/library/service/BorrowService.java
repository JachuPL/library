package com.janbonkowski.library.service;

import com.janbonkowski.library.model.Borrow;
import com.janbonkowski.library.repository.BorrowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BorrowService {
    private BorrowRepository repository;

    @Autowired
    public BorrowService(BorrowRepository repository){
        this.repository = repository;
    }

    public Optional<Borrow> get(int id){ return repository.findById(id); }
    public List<Borrow> getAll() { return (List<Borrow>) repository.findAll(); }
    public void add(Borrow borrow) { repository.save(borrow); }
    public void delete(Borrow borrow) { repository.delete(borrow); }
    public long count() { return repository.count(); }
}
