package com.janbonkowski.library.service;

import com.janbonkowski.library.model.Member;
import com.janbonkowski.library.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MemberService {
    private MemberRepository repository;

    @Autowired
    public MemberService(MemberRepository repository){
        this.repository = repository;
    }

    public Optional<Member> get(int id){ return repository.findById(id); }
    public List<Member> getAll() { return (List<Member>) repository.findAll(); }
    public void add(Member member) { repository.save(member); }
    public void delete(Member member) { repository.delete(member); }
    public long count() { return repository.count(); }
    public void update(Member member) { repository.save(member); }
}
