package com.janbonkowski.library.service;

import com.janbonkowski.library.model.Book;
import com.janbonkowski.library.repository.BookRepository;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {

    private BookRepository repository;

    @Autowired
    public BookService(BookRepository repository){
        this.repository = repository;
    }

    public Optional<Book> get(int id){ return repository.findById(id); }
    public List<Book> getAll() { return (List<Book>) repository.findAll(); }
    public void add(Book book) { repository.save(book); }
    public void delete(Book book) { repository.delete(book); }
    public long count() { return repository.count(); }
    public void update(Book book) { repository.save(book); }
    public long getBorrowedCount()
    {
        List<Book> books = Lists.newArrayList(repository.findAll());
        books = books.stream().filter(x -> !x.getIsLostOrDestroyed()).collect(Collectors.toList());
        return books.size();
    }
    public long getNonBorrowedCount() { return count() - getBorrowedCount(); }
}
