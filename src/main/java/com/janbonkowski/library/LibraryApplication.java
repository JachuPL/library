package com.janbonkowski.library;

import com.janbonkowski.library.model.Book;
import com.janbonkowski.library.model.Borrow;
import com.janbonkowski.library.model.Member;
import com.janbonkowski.library.service.BookService;
import com.janbonkowski.library.service.BorrowService;
import com.janbonkowski.library.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@SpringBootApplication
public class LibraryApplication implements CommandLineRunner {

	private MemberService memberService;
	private BookService bookService;
	private BorrowService borrowService;

	@Autowired
	public LibraryApplication(MemberService memberService,
							  BookService bookService,
							  BorrowService borrowService){
		this.bookService = bookService;
		this.borrowService = borrowService;
		this.memberService = memberService;
	}

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		createAndInsertBooks();
		for(Book book : bookService.getAll())
			System.out.println(book);

		createAndInsertPeople();
		for(Member member : memberService.getAll())
			System.out.println(member);

		createAndInsertSampleBorrows();
		for(Borrow borrow : borrowService.getAll())
			System.out.println(borrow);
		printBorrowPenaltyPrice();
		markAsLost();
		printLostBook();
	}

	private void printLostBook() {
		Book book = bookService.get(1).get();
		System.out.println(book);
	}

	private void markAsLost() {
		Book book = bookService.get(1).get();
		book.markAsLostOrDestroyed();
		bookService.update(book);
	}

	private void printBorrowPenaltyPrice() {
		Borrow borrow = borrowService.get(1).get();
		System.out.println("Przetrzymana? " + borrow.isOverMaximumBorrowPeriod());
		System.out.println("Kwota: $" +borrow.computePenaltyPrice());
	}

	private void createAndInsertBooks(){
		Book book1 = Book.Create("Hory Portier", "J.K.L.M.N.O.P.Q. Rowling", Date.from(LocalDate.now().minusYears(10).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		Book book2 = Book.Create("Grishjarta", "Nattramn", Date.from(LocalDate.now().minusYears(5).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		bookService.add(book1);
		bookService.add(book2);
	}

	private void createAndInsertPeople(){
		Member member1 = Member.Create("Jan", "", "Bońkowski", Date.from(LocalDate.now().minusYears(23).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		Member member2 = Member.Create("Test", "Magic", "Person", Date.from(LocalDate.now().minusYears(99).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		memberService.add(member1);
		memberService.add(member2);
	}

	private void createAndInsertSampleBorrows(){
		Book book1 = bookService.get(1).get();
		Member member1 = memberService.get(1).get();
		Borrow borrow = Borrow.Create(member1, book1, Date.from(LocalDate.now().minusDays(30).atStartOfDay(ZoneId.systemDefault()).toInstant()), null);
		borrowService.add(borrow);
	}
}