package com.janbonkowski.library.model;

import javax.validation.constraints.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id", updatable = false, nullable = false, unique = true)
    private Integer Id;
    @NotNull
    private String title;
    @NotNull
    private String author;
    @NotNull
    @Temporal(TemporalType.DATE)
    @Past
    private Date releaseDate;
    private Boolean isLostOrDestroyed;

    public void markAsLostOrDestroyed(){ this.isLostOrDestroyed = true; }

    public static Book Create(@NotNull String title, @NotNull String author, @Past Date releaseDate){
        return new Book(0, title, author, releaseDate, false);
    }
}
