package com.janbonkowski.library.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Data
@Entity
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true, name = "Id")
    private Integer Id;
    @NotNull
    private String firstName;
    @NotNull
    private String middleName;
    @NotNull
    private String lastName;
    @Temporal(TemporalType.DATE)
    @Past
    private java.util.Date dateOfBirth;

    public static Member Create(@NotNull String firstName, @NotNull String middleName, @NotNull String lastName, @Past java.util.Date dateOfBirth){
        return new Member(0, firstName, middleName, lastName, dateOfBirth);
    }
}
