package com.janbonkowski.library.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Past;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;

import static java.time.temporal.ChronoUnit.DAYS;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Data
@Entity
public class Borrow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id", unique = true, nullable = false, updatable = false)
    private Integer id;
    @ManyToOne
    private Member member;
    @ManyToOne
    private Book book;
    @Temporal(TemporalType.DATE)
    @Past
    private java.util.Date rentalDate;
    @Temporal(TemporalType.DATE)
    @Past
    @Null
    private java.util.Date returnDate;

    public static Borrow Create(@NotNull Member member, @NotNull Book book, @Past java.util.Date rentalDate, @Past java.util.Date returnDate){
        return new Borrow(0, member, book, rentalDate, returnDate);
    }

    public boolean isOverMaximumBorrowPeriod(){
        return LocalDate.now().isAfter(LocalDate.parse(rentalDate.toString()).plusDays(14));
    }

    public double computePenaltyPrice(){
        if (isOverMaximumBorrowPeriod())
            return DAYS.between(LocalDate.parse(rentalDate.toString()).plusDays(14), LocalDate.now()) * 1.99;

        return 0;
    }

}