package com.janbonkowski.library.repository;


import com.janbonkowski.library.model.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends CrudRepository<Member, Integer>{

}