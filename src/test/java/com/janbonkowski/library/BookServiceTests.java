package com.janbonkowski.library;

import com.janbonkowski.library.model.Book;
import com.janbonkowski.library.service.BookService;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceTests {
    @Autowired
    private Book randomBook;

    private BookService service;

    public BookServiceTests(){
        ArrayList<Book> books = new ArrayList<>();
        Book book1 = Book.Create("Test", "Test",new Date());
        Book book2 = Book.Create("Test2", "Test2", new Date());
        books.add(book1);
        books.add(book2);
        service = mock(BookService.class);
        Mockito.when(service.getAll()).thenReturn(books);
        Mockito.when(service.get(1)).thenReturn(java.util.Optional.ofNullable(book1));
        Mockito.when(service.get(2)).thenReturn(java.util.Optional.ofNullable(book2));
    }

    @Test
    public void shouldFindAllBooks(){
        List<Book> books = (List<Book>) service.getAll();
        Assert.assertTrue(books.size() == 2);
    }

    @Test
    public void shouldFindBookById(){
        Optional<Book> book = service.get(1);
        Assert.assertTrue(book != null);

        book = service.get(2);
        Assert.assertTrue(book != null);
    }

    @Test
    public void shouldProduceRandomBook(){
        Assert.assertTrue(randomBook != null);
        System.out.println(randomBook);
    }
}
